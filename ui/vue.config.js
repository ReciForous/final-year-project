module.exports = {
	"devServer": {
		"https": true,
		"proxy": "https://192.169.0.20:5000",
		// "publicPath": ""
	},
	"publicPath": "/static/",
	"outputDir": "../cms/static/",
	"indexPath": '../views/index.html',
	"transpileDependencies": [
	"vuetify"
	],
}
