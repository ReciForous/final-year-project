import Vue from 'vue'
import VueSession from 'vue-session'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify';
import store from './store'
import axios from 'axios'
import VueAxios from 'vue-axios'

var filter = function(text, length, clamp) {
    clamp = clamp || '...';
    var node = document.createElement('div');
    node.innerHTML = text;
    var content = node.textContent;
    return content.length > length ? content.slice(0, length) + clamp : content;
};

Vue.filter('truncate', filter)
Vue.config.productionTip = false
Vue.use(VueAxios, axios.create({
    baseURL: 'https://192.168.0.20:5000/api/v1'
}))
Vue.use(VueSession)
new Vue({
    router,
    vuetify,
    store,
    render: h => h(App)
}).$mount('#app')
