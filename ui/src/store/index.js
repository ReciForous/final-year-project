import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    search: null,
    cart: []
  },
  getters: {
    getCart: state => {
        return state.cart
    }
  },
  mutations: {
    setSearch: (state, newVal) => {
        state.search = newVal
    }
  },
  actions: {
  },
  modules: {
  }
})
