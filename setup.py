from setuptools import setup

setup(
    name='SearchUs',
    packages=['cms'],
    version='1.0',
    install_requires=[
        'Flask',
        'Flask-SQLAlchemy',
        'Flask-CORS',
        'Flask-Ext',
        'click',
        'crayons',
        'gunicorn',
        'tensorflow',
        'python-dotenv',
        'psycopg2',
        'sqlalchemy-searchable',
        'pyopenssl'
    ]
)
