import os
import click
import crayons

from flask import (
    Flask
)

app = Flask(
    __name__,
    template_folder='views'
)

from flask_cors import CORS


UPLOAD_FOLDER = os.environ.get(
    'UPLOAD_FOLDER',
    '/data/images/'
)
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif', 'docx', 'svg'])

app.config['SECRET_KEY'] = os.environ.get(
    'APP_KEY',
    'KGcC-qBv8JEhQazKoBfxUq2efFzEZGHTobW4sdnzAMs'
)

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgres://postgres:postgres@0.0.0.0:5432/db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

from cms.controllers.api import api
from cms.controllers.site import site


@app.cli.command('dbinit')
def dbinit():
    from cms.models import db
    click.echo('Creating tables and search indexes')
    db.configure_mappers()
    db.create_all()
    click.echo('DONE')


@app.cli.command('dropdb')
def dropdb():
    from cms.models import db
    click.echo('Dropping db')
    db.reflect()
    db.drop_all()
    click.echo('Done')


@app.cli.command('loaddb')
def loaddb():
    from cms.importers import mass_add_products
    from cms.importers import mass_add_sellers
    from cms.importers import mass_add_associations

    mass_add_products('products.csv')
    mass_add_sellers('sellers.csv')
    mass_add_associations('associations.csv')

    click.echo('DONE')


@app.cli.command('search-test')
def test():
    from cms.models.product import Product

    search = 'asus'

    products = Product.search(search)
    for product in list(products.items):
        print(product.name)
        if product.sellers:
            for seller in product.sellers:
                print(seller.seller.name)
                print(seller.price)


cors = CORS(
    app,
    resources={
        r"/api/*": {
            "origins": "*"
        }
    }
)

app.register_blueprint(
    site,
    url_prefix='/'
)

app.register_blueprint(
    api,
    url_prefix='/api/v1'
)


if __name__ == '__main__':
    app.run(
        host='0.0.0.0',
        port=5000
    )
