from cms import app
from cms.helper import Message

import os

from sqlalchemy_searchable import make_searchable
from flask import abort
from flask_sqlalchemy import(
    SQLAlchemy
)


db = SQLAlchemy(app)
make_searchable(db.metadata)
