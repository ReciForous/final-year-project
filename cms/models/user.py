import click
import crayons

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy_utils.types import TSVectorType

from cms.models import db
from cms.helper import Message


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text, nullable=False)
    address = db.Column(db.Text, nullable=False)
    phone = db.Column(db.Text, nullable=False)
    email = db.Column(db.Text, nullable=False)
    password = db.Column(db.Text, nullable=False)
