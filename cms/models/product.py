from cms.helper import Message
from cms.models import db

import click
import crayons

from flask_sqlalchemy import SQLAlchemy, BaseQuery
from sqlalchemy_searchable import SearchQueryMixin
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy_utils.types import TSVectorType


class ProductQuery(BaseQuery, SearchQueryMixin):
    pass


# Parent
class Product(db.Model):
    query_class = ProductQuery
    __tablename__ = 'products'

    id = db.Column(db.Integer, primary_key=True)
    code = db.Column(db.UnicodeText, unique=True, nullable=False)
    name = db.Column(db.UnicodeText, nullable=False)
    description = db.Column(db.UnicodeText)
    tags = db.Column(db.UnicodeText, nullable=False)
    image = db.Column(db.UnicodeText)
    search_vector = db.Column(TSVectorType(
        'name',
        'code',
        'description',
        'tags',
    ))
    sellers = db.relationship("Association", back_populates='product')

    def get(id):
        return Product.query.get(id)

    def getAll():
        return Product.query.order_by(Product.id).all()

    def search(query, page=1, max=15):
        return Product.query.search(query).paginate(
            page=page,
            per_page=max,
            error_out=True
        )

    def add(self):
        if not (self.name or self.code or self.tags):
            return Message(
                status=False,
                content='Error: Missing required fields'
            )

        try:
            db.session.add(self)
            db.session.commit()
            message = Message(
                status=True,
                content='Product added successfully',
            )
        except SQLAlchemyError as e:
            db.session.rollback()
            message = Message(
                status=False,
                content=f'Error: {e}'
            )
        finally:
            db.session.close()
            click.echo(crayons.green(message.content) if message.status else crayons.red(message.content))
            return message
