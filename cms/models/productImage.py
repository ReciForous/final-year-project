import click
import crayons

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy_utils.types import TSVectorType

from cms.models import db
from cms.helper import Message


# Table for MLP to associate images with data
class ProductImage(db.Model):
    __tablename__ = 'productImages'

    id = db.Column(db.Integer, primary_key=True)
    product_id = db.Column(db.Integer, db.ForeignKey('products.id'))
    image = db.Column(db.Text, unique=True)

    def add(self):
        if not self.product_id or self.image:
            return Message(
                status=False,
                content='Error: Missing required fields'
            )
        try:
            db.session.add(self)
            db.session.commit()
            message = Message(
                status=True,
                content='Image added successfully'
            )
        except SQLAlchemyError as e:
            db.session.rollback()
            message = Message(
                status=False,
                content=f'Error: {e}'
            )
        finally:
            click.echo(crayons.green(message.content) if message.status else crayons.red(message.content))
            db.session.close()
            return message.content

    def getByImage(image=None):
        if not image:
            raise AttributeError('Missing required field')

        return ProductImage.query.filter_by(image=image)

