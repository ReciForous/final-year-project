import click
import crayons

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy_utils.types import TSVectorType

from cms.models import db
from cms.helper import Message


class Association(db.Model):
    __tablename__ = 'association'
    id = db.Column(
        db.Integer,
        unique=True,
        primary_key=True,
        autoincrement=True
    )
    left_id = db.Column(
        db.Integer,
        db.ForeignKey('products.id'),
        primary_key=True
    )
    right_id = db.Column(
        db.Integer,
        db.ForeignKey('sellers.id'),
        primary_key=True
    )
    price = db.Column(
        db.DECIMAL,
        nullable=False
    )
    seller = db.relationship('Seller', back_populates='products')
    product = db.relationship('Product', back_populates='sellers')

    def add(self):
        if not (self.seller or self.product or self.price):
            return Message(
                status=False,
                content='Error: Missing required fields'
            )

        try:
            db.session.add(self)
            db.session.commit()
            message = Message(
                status=True,
                content='Association add successfully'
            )
        except SQLAlchemyError as e:
            db.session.rollback
            message = Message(
                status=False,
                content=f'Error: {e}'
            )
        finally:
            db.session.close()
            click.echo(crayons.green(message.content) if message.status else crayons.red(message.content))
            return message
