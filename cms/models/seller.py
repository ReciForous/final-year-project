import click
import crayons

from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy_utils.types import TSVectorType

from cms.models import db
from cms.helper import Message


# Child
class Seller(db.Model):
    __tablename__ = 'sellers'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.Text)
    code = db.Column(db.Text, unique=True, nullable=False)
    verified = db.Column(db.Boolean, default=False)
    products = db.relationship('Association', back_populates="seller")

    def get(id):
        return Seller.query.get(id)

    def getAll():
        return Seller.query.order_by(Seller.id).all()

    def add(self):
        if not (self.name or self.code):
            return Message(
                status=False,
                content='Error: Missing required fields',
            )

        try:
            db.session.add(self)
            db.session.commit()
            message = Message(
                status=True,
                content='Added successfully'
            )
        except SQLAlchemyError as e:
            db.session.rollback()
            message = Message(
                status=False,
                content=f'Error: {e}'
            )
        finally:
            db.session.close()
            click.echo(crayons.green(message.content) if message.status else crayons.red(message.content))
            return message
