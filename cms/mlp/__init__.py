from __future__ import (
    absolute_import,
    division,
    print_function,
    unicode_literals
)

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import (
    Dense,
    Conv2D,
    Flatten,
    Dropout,
    MaxPooling2D
)
from tensorflow.keras.preprocessing.image import ImageDataGenerator

import os
import numpy as np
import matplotlib.pyplot as plt

print(tf.__version__)

data_dir = os.path.join(os.path.dirname(__file__), 'data')

training_dir = os.path.join(data_dir, 'training')
validation_dir = os.path.join(data_dir, 'validation')

# training_laptops_dir = os.path.join(training_dir, 'laptops')
# validation_laptops_dir = os.path.join(training_dir, 'laptops')

train_macbook_dir = os.path.join(training_dir, 'macbook')
train_asus_dir = os.path.join(training_dir, 'asus rog')
train_msi_dir = os.path.join(training_dir, 'msi stealth')
train_razer_dir = os.path.join(training_dir, 'razer blade 15')
# train_laptops_dir = os.path.join(training_dir, 'laptops')

validate_macbook_dir = os.path.join(validation_dir, 'macbook')
validate_asus_dir = os.path.join(validation_dir, 'asus rog')
validate_msi_dir = os.path.join(validation_dir, 'msi stealth')
validate_razer_dir = os.path.join(validation_dir, 'razer blade 15')
# validate_laptops_dir = os.path.join(validation_dir, 'laptops')

num_mac_tr = len(os.listdir(train_macbook_dir))
num_asus_tr = len(os.listdir(train_asus_dir))
num_msi_tr = len(os.listdir(train_msi_dir))
num_razer_tr = len(os.listdir(train_razer_dir))
# num_laptops_tr = len(os.listdir(train_laptops_dir))
print(f'No of training macbook images: {num_mac_tr}')
print(f'No of training asus images: {num_asus_tr}')
print(f'No of training msi images: {num_msi_tr}')
print(f'No of training razer images: {num_razer_tr}')
# print(f'No of training laptop images: {num_laptops_tr}')
print('---------------------------------------')

num_mac_val = len(os.listdir(validate_macbook_dir))
num_asus_val = len(os.listdir(validate_asus_dir))
num_msi_val = len(os.listdir(validate_msi_dir))
num_razer_val = len(os.listdir(validate_razer_dir))
# num_laptops_val = len(os.listdir(validate_laptops_dir))
print(f'No of validation macbook images: {num_mac_val}')
print(f'No of validation asus images: {num_asus_val}')
print(f'No of validation msi images: {num_msi_val}')
print(f'No of validation razer images: {num_razer_val}')
# print(f'No of validation laptops images: {num_laptops_val}')

total_train = num_mac_tr + num_asus_tr + num_msi_tr + num_razer_tr
total_val = num_mac_val + num_asus_val + num_msi_val + num_razer_val

print('---------------------------------------')
print(f'Total training images: {total_train}')
print(f'Total validation images: {total_val}')

batch_size = 128
epochs = 15
IMG_HEIGHT = 150
IMG_WIDTH = 150

train_image_generator = ImageDataGenerator(
    rescale=1./255
)
validation_image_generator = ImageDataGenerator(rescale=1. / 255)

train_data_gen = train_image_generator.flow_from_directory(
    batch_size=batch_size,
    directory=training_dir,
    shuffle=True,
    target_size=(IMG_HEIGHT, IMG_WIDTH),
    class_mode='binary'
)

valid_data_gen = validation_image_generator.flow_from_directory(
    batch_size=batch_size,
    directory=validation_dir,
    target_size=(IMG_HEIGHT, IMG_WIDTH),
    class_mode='binary'
)

sample_training_images, _ = next(train_data_gen)


def plotImages(images_arr):
    fig, axes = plt.subplots(1, 5, figsize=(20,20))
    axes = axes.flatten()
    for img, ax in zip( images_arr, axes):
        ax.imshow(img)
        ax.axis('off')
    plt.tight_layout()
    plt.show()


plotImages(sample_training_images[:5])

model = Sequential([
    Conv2D(16, 3, padding='same', activation='relu', input_shape=(IMG_HEIGHT, IMG_WIDTH ,3)),
    MaxPooling2D(),
    Conv2D(32, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Conv2D(64, 3, padding='same', activation='relu'),
    MaxPooling2D(),
    Flatten(),
    Dense(512, activation='relu'),
    Dense(1, activation='sigmoid')
])

model.compile(
    optimizer='adam',
    loss='binary_crossentropy',
    metrics=['accuracy']
)

model.summary()

history = model.fit_generator(
    train_data_gen,
    steps_per_epoch=total_train // batch_size,
    epochs=epochs,
    validation_data=valid_data_gen,
    validation_steps=total_val // batch_size
)

# print(history.history)

acc = history.history['acc']
val_acc = history.history['val_acc']

loss = history.history['loss']
val_loss = history.history['val_loss']

epochs_range = range(epochs)

plt.figure(figsize=(8, 8))
plt.subplot(1, 2, 1)
plt.plot(epochs_range, acc, label='Training Accuracy')
plt.plot(epochs_range, val_acc, label='Validation Accuracy')
plt.legend(loc='lower right')
plt.title('Training and Validation Accuracy')

plt.subplot(1, 2, 2)
plt.plot(epochs_range, loss, label='Training Loss')
plt.plot(epochs_range, val_loss, label='Validation Loss')
plt.legend(loc='upper right')
plt.title('Training and Validation Loss')
plt.show()
