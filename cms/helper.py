import os
from datetime import datetime
from collections import namedtuple
from functools import wraps
import csv

import click
import crayons

from flask import (
    Flask,
    flash,
    request,
    redirect,
    url_for,
    session
)
from werkzeug.utils import secure_filename

from cms import (
    app,
    ALLOWED_EXTENSIONS
)

Message = namedtuple('Message', ['status', 'content'])


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS


def upload_file(request, file_id='file'):
    # check if the post request has the file part
    if file_id not in request.files:
        message = Message(
            status=False,
            content='File not in request.',
            category='error'
        )
        return message

    files = request.files.getlist(file_id, None)

    messages = []

    for file in files:
        # if user does not select file, browser also
        # submit an empty part without filename
        if file.filename == '':
            message = Message(
                status=False,
                content='File not in request.',
                category='error'
            )
            return message
        if file and allowed_file(file.filename):
            filename = file.filename.split('.')
            filename = secure_filename(
                str(filename[:-1])
                + '_'
                + str(datetime.utcnow().strftime('%Y-%m-%dT%H-%M-%S'))
                + '.'
                + str(filename[-1])
            )

            file.save(os.path.join(
                (
                    os.path.dirname(os.path.abspath(__file__))
                    + app.config['UPLOAD_FOLDER']
                ),
                filename
             ))
            message = Message(
                status=True,
                content=str(os.path.join(app.config['UPLOAD_FOLDER'], filename)),
                category='success'
            )

            messages.append(message)

    if len(messages) > 1:
        return messages
    else:
        return messages[0]


def saveFile(file, extension):
    # get file path relative to this file
    file_path = os.path.dirname(os.path.abspath(__file__)) + '/data/images/'
    # Generate file name as utcnow, will fail at some point on multiple uploads
    file_name = str(datetime.utcnow()) + extension

    # create full path
    full_path = file_path + file_name

    # Create file and open in write binary mode
    save_to = open(full_path, 'wb')
    # Write content
    save_to.write(file)
    # close file
    save_to.close()
    # return file path
    return file_name


def login_required(permission=None):
    def real_login_required(f):
        @wraps(f)
        def decorated_function(*args, **kwargs):
            user = session.get('user', None)
            if not user:
                return redirect(
                    url_for(
                        'admin.login',
                        next=request.url
                    )
                )
            if isinstance(permission, str):
                permissions = [permission, ]
            else:
                permissions = permission
            if (
                permissions
                and not any(permission in permissions for permission in user['roles'])
            ):

                message = Message(
                    status=False,
                    content='Sorry you do not have permissions to view that page',
                    category='error')
                click.echo(crayons.red(message.content), err=True)
                flash(message.content, message.category)
                return redirect(
                        url_for(
                            'admin.portal'
                        )
                    )
            return f(*args, **kwargs)
        return decorated_function
    return real_login_required
