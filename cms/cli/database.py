import click
import crayons


@click.command()
def initdb():
    from cms.models import db
    click.echo('Creating tables and search indexes')
    db.configure_mappers()
    db.create_all()
    click.echo('DONE')


@click.command()
@click.confirmation_option(
    prompt='Are you sure you want to drop the database?'
)
def dropdb():
    from cms.models import db
    click.echo('Dropping all table structures and search indexes...')
    db.reflect()
    db.drop_all()
    click.echo('DONE')


@click.command()
def sampledb():
    from sqlalchemy.exc import SQLAlchemyError
    from cms.models import db
    from cms.models.product import Product
    from cms.models.seller import Seller
    from cms.models.association import Association

    click.echo('Creating sample data')
    c1 = Seller(name='Hypoxia', code='hypxi-3412', verified=True)
    c1.add()

    c2 = Seller(name='RoomLit', code='rmmlit-33122')
    c2.add()

    c3 = Seller(name='GoodLife', code='gdlif-455123', verified=True)
    c3.add()

    c4 = Seller(name='Jimmies', code='jims-3123', verifeid=True)
    c4.add()

    c5 = Seller(name='Gordons Hell', code='gdh-9099')
    c5.add()

    p1 = Product(
        name='MacBook Pro 16"',
        description='Only for the rich with an extra two kidneys to sell',
        tags=['laptop', 'macbook', 'electronics', 'mac', 'computers']
    )
    p1.add()

    p2 = Product(
        name='ASUS ROG Zephyrus GX501',
        description='For the uber gamebois bought with daddies money',
        tags=['laptop', 'gaming', 'electronics', 'mac', 'computers', 'nvidia', 'geforce', 'rtx', 'gtx']
    )
    p2.add()

    p3 = Product(
        name='MSI GS65 Stealth',
        description='Damn, you make me drool',
        tags=['laptop', 'gaming', 'electronics', 'mac', 'computers', 'nvidia', 'geforce', 'rtx', 'gtx']
    )
    p3.add()

    p4 = Product(
        name='Razer Blade 15',
        description='THE PERFECT LAPTOP!!',
        tags=['laptop', 'gaming', 'electronics', 'mac', 'computers', 'nvidia', 'geforce', 'rtx', 'gtx']
    )
    p4.add()

    assoc1 = Association(price=1500)
    assoc1.seller = c1

    assoc2 = Association(price=1499.99)
    assoc2.seller = c2

    assoc3 = Association(price=1199.99)
    assoc3.seller = c3

    assoc4 = Association(price=2000)
    assoc4.seller = c4
    try:
        p1.sellers.extend([assoc1, assoc2, assoc3, assoc4])
        db.commit()
    except SQLAlchemyError as e:
        click.echo(crayons.red(e))

    assoc1 = Association(price=1500)
    assoc1.seller = c1

    assoc2 = Association(price=1499.99)
    assoc2.seller = c2

    assoc3 = Association(price=1199.99)
    assoc3.seller = c5

    assoc4 = Association(price=2000)
    assoc4.seller = c3
    try:
        p2.sellers.extend([assoc1, assoc2, assoc3, assoc4])
        db.commit()
    except SQLAlchemyError as e:
        click.echo(crayons.red(e))

    assoc1 = Association(price=1200)
    assoc1.seller = c1

    assoc2 = Association(price=1300.99)
    assoc2.seller = c2

    assoc3 = Association(price=1000.99)
    assoc3.seller = c5

    assoc4 = Association(price=1000)
    assoc4.seller = c3
    try:
        p3.sellers.extend([assoc1, assoc2, assoc3, assoc4])
        db.commit()
    except SQLAlchemyError as e:
        click.echo(crayons.red(e))

    assoc1 = Association(price=3000)
    assoc1.seller = c1

    assoc2 = Association(price=2500)
    assoc2.seller = c2

    assoc3 = Association(price=2500)
    assoc3.seller = c5

    assoc4 = Association(price=2000)
    assoc4.seller = c3
    try:
        p4.sellers.extend([assoc1, assoc2, assoc3, assoc4])
        db.commit()
    except SQLAlchemyError as e:
        click.echo(crayons.red(e))

    click.echo('DONE')
