import os
import csv

from cms.models.product import Product
from cms.models.seller import Seller
from cms.models.association import Association


def mass_add_products(filename):
    filepath = os.path.join(os.path.dirname(__file__), 'data/' + filename)
    # print(filepath)
    with open(filepath, 'r', encoding='utf-8') as csv_file:
        reader = csv.reader(csv_file)

        next(reader, None)
        for row in reader:
            name = row[0]
            code = row[1]
            description = row[2]
            tags = list(row[3].split())
            Product(
                name=name,
                code=code,
                description=description,
                tags=tags
            ).add()


def mass_add_sellers(filename):
    filepath = os.path.join(os.path.dirname(__file__), 'data/' + filename)
    with open(filepath, 'r', encoding='utf-8') as csv_file:
        reader = csv.reader(csv_file)

        next(reader, None)
        for row in reader:
            name = row[0]
            code = row[1]
            try:
                verified = True if row[2] else False
            except IndexError as e:
                verified = False
            Seller(
                name=name,
                code=code,
                verified=verified
            ).add()


def mass_add_associations(filename):
    filepath = os.path.join(os.path.dirname(__file__), 'data/' + filename)

    with open(filepath, 'r', encoding='utf-8') as csv_file:
        reader = csv.reader(csv_file)

        next(reader, None)
        for row in reader:
            Association(
                seller=Seller.get(int(row[0])),
                product=Product.get(int(row[1])),
                price=float(row[2])
            ).add()
