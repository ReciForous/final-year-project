from flask import (
    Blueprint,
    jsonify,
    request,
    render_template
)

from cms.models import db
from cms import app
from cms.models.product import Product
from cms.models.productImage import ProductImage
from cms.models.association import Association
from cms.models.seller import Seller
from cms.helper import saveFile

site = Blueprint(
    'site',
    '__name__'
)


@site.route('/', defaults={'path': ''})
@site.route('/<path:path>', methods=['GET'])
def index(path):
    return render_template('index.html')
