import os
import base64
import click
import crayons
from datetime import datetime
from collections import defaultdict
import secrets

from flask import (
    Blueprint,
    jsonify,
    request
)
from random import randint

from cms.models import db
from cms import app
from cms.models.product import Product
from cms.models.productImage import ProductImage
from cms.models.association import Association
from cms.models.seller import Seller
from cms.helper import saveFile

api = Blueprint(
    'api',
    '__name__'
)

# Following REST implementation
@api.route('/products', methods=['GET', 'POST'])
def products():
    if request.method == 'POST':
        data = request.get_json()

        image = data['image']

        product = Product(
            name=data['name'],
            code=data['code'],
            tags=data['tags'],
            description=data['description'],
        )

        message = product.add()

        rv = {
            'status': message.status,
            'content': message.content,
            'product': {
                'id': product.id,
                'name': product.name,
                'code': product.code,
                'description': product.description,
                'tags': product.tags,
                'image': product.image
            }
        }

        return jsonify(rv)
    else:
        return jsonify({
            'status': False,
            'content': 'Under Construction'
        })


@api.route('/products/<int:id>', methods=['GET', 'PUT', 'DELETE'])
def product(id):
    if request.method == 'PUT':
        data = request.get_json()
        # Do update
    else:
        if request.method == 'GET':
            product = Product.get(id)
            click.echo(product)
            return jsonify(
                {
                    'id': product.id,
                    'name': product.name,
                    'description': product.description,
                    'code': product.code,
                    'sellers': [
                        {
                            "name": seller.seller.name,
                            "code": seller.seller.code,
                            "verified": seller.seller.verified,
                            "price": float(seller.price)
                        } for seller in product.sellers
                    ]
                }
            )
        else:
            # Do delete
            pass


@api.route('/search', methods=['POST'])
def search():
    data = request.get_json()
    try:
        image = data['image'].split(',')[1]
        image = base64.b64decode(image)
        extension = data['extension']

        filename = saveFile(image, extension)
        # Give for classification and get results\

        # products = Product.search(mlpResult)
        # products = [{
        #     'id': product.id,
        #     'name': product.name,
        #     'code': product.code,
        #     'description': product.description,
        #     'sellers': [
        #         {
        #             'name': seller.seller.name,
        #             'code': seller.seller.code,
        #             'price': str(seller.price)
        #         } for seller in product.sellers
        #     ]
        # } for product in list(products.items)]
        return jsonify({
            'image': filename,
            'extension': extension
        })
    except KeyError as e:
        click.echo('Image key missing default to search key')
        search = data['search']
        products = []
        for search_term in search:
            products += list(Product.search(search_term).items)

        products = [{
            'id': product.id,
            'name': product.name,
            'code': product.code,
            'description': product.description,
            'sellers': [
                {
                    'name': seller.seller.name,
                    'code': seller.seller.code,
                    'price': float(seller.price)
                } for seller in product.sellers
            ]
        } for product in products]
        return jsonify(products)


@api.route('/trending', methods=['GET'])
def trending():
    products = Product.getAll()
    randomProducts = [products[randint(0, len(products) - 1)] for i in range(3)]
    randomProducts = [{
        "id": product.id,
        "name": product.name,
        "featuredSeller": {
            "name": product.sellers[0].seller.name,
            "price": float(product.sellers[0].price)
        }
    } for product in randomProducts]
    return jsonify(randomProducts)


@api.route('/onSale', methods=['GET'])
def onSale():
    products = Product.getAll()
    randomProducts = [products[randint(0, len(products) - 1)] for i in range(3)]
    randomProducts = [{
        "id": product.id,
        "name": product.name,
        "featuredSeller": {
            "name": product.sellers[0].seller.name,
            "price": float(product.sellers[0].price)
        }
    } for product in randomProducts]
    return jsonify(randomProducts)


@api.route('/featured', methods=['GET'])
def featured():
    products = Product.getAll()
    randomProducts = [products[randint(0, len(products) - 1)] for i in range(3)]
    randomProducts = [{
        "id": product.id,
        "name": product.name,
        "featuredSeller": {
            "name": product.sellers[0].seller.name,
            "price": float(product.sellers[0].price)
        }
    } for product in randomProducts]
    return jsonify(randomProducts)
