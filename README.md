# SearchUs

SearchUs - A product cataloging website which uses real-time image recognition to find products in the SearchUs database

**Technical Stack**

* Python3
* Flask
* Flask-SQLAlchemy
* Postgres (Docker image)
* Vue JS
* TensorFlowLite (tfjs)
* coco-ssd TensorFlow model

### Prerequisites

This guide assumes you have the following packages installed on your system.

* Python3
* pip3
* Node
* npm
* Docker
* docker-compose
* psql-client
* psql-devel tools

### Setup

**Python Backend**

```
# From root dir
# Create a python virtual environment and setup dependencies
$ pipenv run python3 setup.py

# Run one more time in case any dependencies fail to install first time
$ pipenv run python3 setup.py

# Serve flask dev server
$ pipenv run flask run --host=0.0.0.0 --cert=cert.pem --key=key.pem
```

*Note:* Tensorflow library will take some time to install as its the biggest dependency of about 100Mb

#### Setting Up Postgres

```
# From root dir
$ cd postgres

''' 
Starts up containerd docker service from docker image if
docker image is not present it will be downloaded
'''
$ docker-compose up -d
```

#### Setting Up Vue JS Frontend

```
# From root dir
$ cd ui

# Install node modules
$ npm install

# Serve vue development server
$ npm run serve

# To make your life easier
$ npm install --global @vue/cli
```

### Populating Database

Dummy test files are provided in cms/data/ in the form of csv files.

* products.csv
* sellers.csv
* associations.csv

#### Create tables with flask cli

```
$ pipenv run flask dbinit
```

#### Load data to tables with flask cli

```
$ pipenv run flask loaddb
```

#### Drop table if needed

```
$ pipenv run flask dropdb
```

#### Test if table populated properly

```
$ pipenv run flask search-test
```
